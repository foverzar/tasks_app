from django.db import models


class Task(models.Model):
    priority = models.IntegerField(db_index=True, default=0)
    name = models.CharField(db_index=True, max_length=200)
    description = models.TextField(db_index=True)
