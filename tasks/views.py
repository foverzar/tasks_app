from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Task
import json
import csv
from io import StringIO

# Create your views here.


def index(request):
    if request.method == 'GET':
        if 'text/html' in request.META.get('HTTP_ACCEPT'):  # This is a browser
            return render(request, 'index.html')
        else:  # Handle as json request
            order_by_fields_str = request.GET.get('order_by') # ?order_by=-priority;name; (Minus means desc)
            filter_settings = dict()
            priority_str = request.GET.get('priority')
            description_str = request.GET.get('description')
            name_str = request.GET.get('name')

            if priority_str is not None:
                filter_settings.update({'priority': priority_str})
            if description_str is not None:
                filter_settings.update({'description__icontains': description_str})
            if name_str is not None:
                filter_settings.update({'name__icontains': name_str})

            query_buf = Task.objects.all()

            if len(filter_settings) != 0:
                query_buf = query_buf.filter(**filter_settings)

            if order_by_fields_str is not None:
                query_buf = query_buf.order_by(*(order_by_fields_str.split(';')[:-1]))

            offset = int(request.GET.get('offset',0))
            ammount = request.GET.get('ammount')
            
            if ammount is not None:
                query_buf = query_buf.all()[offset:offset+int(ammount)]

            return JsonResponse(list(query_buf.values()), safe=False)

    if request.method == 'POST':
        task_list = []
        if 'text/csv' in request.META.get('CONTENT_TYPE'):
            is_csv = True
            reader = csv.reader(StringIO(request.body.decode('utf-8')), delimiter=',')
            for row in reader:
                task_list.append(Task(priority=row[0], name=row[1], description=row[2]))
        else: # If it's not explictly defined as csv, fallback to json as default
            data_raw = request.body.decode('utf-8')
            data = json.loads(data_raw)
            if isinstance(data, list):
                task_list = []
                for entry in data:
                    task_list.append(
                            Task(priority=entry['priority'], name=entry['name'], description=entry['description'])
                            )
            else:
                task_list.append(
                        Task(priority=data['priority'], name=data['name'], description=data['description'])
                        )

        if (len(task_list) > 1): # Assuming multiple tasks can be imported only via api in a bulk way.
            Task.objects.bulk_create(task_list)
        else:
            task_list[0].save()

        return HttpResponse(status=200)

    if request.method == 'DELETE':
        priority_gt_val = request.GET.get('priority_gt') # url params get parsed into GET dict 
        priority_lt_val = request.GET.get('priority_lt')
        priority_val = request.GET.get('priority')
        description_val = request.GET.get('description')
        name_val = request.GET.get('name')
        id_val = request.GET.get('id')
        id_gt_val = request.GET.get('id_gt')
        id_lt_val = request.GET.get('id_lt')
        
        filter_settings = dict()

        if priority_gt_val is not None:
            filter_settings.update({'priority__gt': priority_gt_val})
        if priority_lt_val is not None:
            filter_settings.update({'priority__lt': priority_lt_val})
        if priority_val is not None:
            filter_settings.update({'priority__exact': priority_val})
        if description_val is not None:
            filter_settings.update({'description__exact': description_val})
        if name_val is not None:
            filter_settings.update({'name__exact': name_val})
        if id_val is not None:
            filter_settings.update({'pk__exact': id_val})
        if id_gt_val is not None:
            filter_settings.update({'pk__gt': id_gt_val})
        if id_lt_val is not None:
            filter_settings.update({'pk__lt': id_lt_val})


        if len(filter_settings) != 0:
            Task.objects.filter(**filter_settings).delete()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=400)

