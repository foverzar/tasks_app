# Установка
Приложение разрабатывалось с python 3.5.2 и node 6.10.1 -- рекомендуется использовать virtualenv

## Установка зависмостей
```
pip install -r requirements.txt && npm install
```

## Первичная конфигурация
```
./manage.py makemigrations && ./manage.py migrate && npm run build
```

Конфиг базы данных живёт в ./tasks_app/settings.py и по умолчанию тыкается в БД "gett_test" с именем пользователя и паролем "gett_test/gett_test". Приложение слушает на 8000 порту.a

#How to

При отправке GET-запроса в контекст /tasks/ с установленным заголовком "Accept: text/html" (например через браузер) Django возвращает вебапп на React. Если такой заголовок не установлен, запрос расценивается как запрос json-данных:

```
curl -X GET http://localhost:8000/?description=ABC
```

Данный запрос эквивалентен:

```
SELECT * FROM TASK WHERE DESCRIPTION LIKE 'ABC';
```

Через POST-запрос можно отправлять JSON:
```
curl -X POST -d '{"priority":4,"name":"Покрасить забор","description":"Покрасить забор вокруг дома розовым цветом до полуночи сегодня."}' http://localhost:8000/tasks/

curl -X POST -d '[{"priority":1,"name":"ABC","description":"This is a description"},{"priority":2,"name":"another name","description":"Another description"}]' http://localhost:8000/tasks/
```
Если выставить заголовок "Content-Type: text/csv", приложение будет парсить тело запроса как CSV.

Через DELETE запрос можно выполнять удаление задач по условию:
```
curl -X DELETE http://localhost:8000/tasks/?priority_lt=5&priority_gt=2 
```
Данный запрос удалит все задачи с приоритетом 2 &lt; N &gt; 5

Доступны следующие модификаторы:

* priority_gt -- приоритет больше чем
* priority_lt -- приоритет меньше чем
* priority -- приоритет равный
* description -- описание равное
* name -- название равное
* id -- первичный ключ равный
* id_lt -- первичный ключ меньше чем
* id_gt -- первичный ключ больше чем
