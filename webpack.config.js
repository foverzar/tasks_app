const webpack = require('webpack'),
      path = require('path');
      BundleTracker = require('webpack-bundle-tracker');

const BUILD_DIR = path.resolve(__dirname, './assets/dist'),
      APP_DIR = path.resolve(__dirname, './assets/src');

var config = {
  context: __dirname,
  entry: path.resolve(APP_DIR, 'index.js'),
  devtool: '#source-map',
  output: {
    path: BUILD_DIR,
    filename: '[name]-[hash].js',
    publicPath: '/static/',
  },
  module: {
    loaders: [
      {
        test: /\.js$/, loader: 'babel-loader'
      },
      {
        test:/\.css$/, loader: "style-loader!css-loader"
      },
      { 
        test: /\.png$/, 
        loader: "url-loader?limit=100000" 
      },
      { 
        test: /\.jpg$/, 
        loader: "file-loader" 
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, 
        loader: 'url-loader?limit=1Settings000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, 
        loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, 
        loader: 'file-loader'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, 
        loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.css']
  },
  plugins: [
    new BundleTracker({filename: './webpack-stats.json'}),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),
  ],
}

module.exports = config;
