require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/css/bootstrap-theme.css');
require('bootstrap/dist/js/bootstrap.js');
require('./style.css');

const React = require('react'),
      ReactDOM = require('react-dom'),
      App = require('./components/App');


ReactDOM.render(
  <App/>,
  document.getElementById('root')
);
