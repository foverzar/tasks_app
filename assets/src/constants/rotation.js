const rotation = {
    NONE: undefined,
    ASC: 0,
    DESC: 1,
};

module.exports = rotation;
