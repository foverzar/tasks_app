const React = require('react'),
      Sth = require('./SmartTableHeader.js'),
      rotation = require('../constants/rotation');

const TasksTableHead = React.createClass({

    inputHandler: function (dataType, e) {
        const value = e.target.value||"";
        let updatedSettings = {};
        updatedSettings[dataType] = value;
        this.props.settingsHandler(updatedSettings);
    },

  sortingHandler: function (dataType, direction, e) {
    let updatedSettings = {};
    updatedSettings["order_by"] = this.props.orderBy||{};

    switch(direction) {
        case rotation.NONE:
            updatedSettings.order_by[dataType] = rotation.ASC;
            break;
        case rotation.ASC:
            updatedSettings.order_by[dataType] = rotation.DESC;
            break;
        case rotation.DESC:
            updatedSettings.order_by[dataType] = rotation.NONE;
            break;
    }

    this.props.settingsHandler(updatedSettings);
  },

    render: function() {
        let sortDirection = this.props.orderBy||{};
            return (
                <div className="navbar-fixed-top navbar navbar-default">
                    <div className="container-fluid">
                        <div className="panel-heading">
                            <div className="col-xs-1"/>
                            <Sth className="col-xs-2" sortDirection={sortDirection.priority} onClick={this.sortingHandler.bind(null, "priority", sortDirection.priority)}>Приоритет</Sth>
                            <Sth className="col-xs-3" sortDirection={sortDirection.name} onClick={this.sortingHandler.bind(null, "name", sortDirection.name)}>Название</Sth>
                            <Sth className="col-xs-6" sortDirection={sortDirection.description} onClick={this.sortingHandler.bind(null, "description", sortDirection.description)}>Описание</Sth>
                        </div>
                        <div className="panel-body">
                            <div className="col-xs-1"><span onClick={this.props.saveHandler} className="glyphicon glyphicon-plus" /></div>
                            <div className="col-xs-2"><input type="number" value={this.props.priority} onChange={this.inputHandler.bind(null, "priority")} /></div>
                            <div className="col-xs-3"><input type="text" value={this.props.name} onChange={this.inputHandler.bind(null, "name")} /></div>
                            <div className="col-xs-6"><input type="text" value={this.props.description} onChange={this.inputHandler.bind(null, "description")} /></div>
                        </div>
                    </div>
                </div>
            );
    }
});

module.exports = TasksTableHead;
