const React = require('react');

const Task = React.createClass({
  render: function() {
    return (
      <li className="list-group-item container-fluid">
        <div className="col-xs-1"><span className="glyphicon glyphicon-trash" onClick={this.props.deleteHandler} /></div>
	    <div className="col-xs-2">{this.props.priority}</div>
        <div className="col-xs-3">{this.props.name}</div>
        <div className="col-xs-6">{this.props.description}</div>
      </li>
    );
  }
});

module.exports = Task;
