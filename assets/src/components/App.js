const React = require('react'),
      TasksTableHead = require('./TasksTableHead'),
      TasksTableBody = require('./TasksTableBody'),
      queryString = require('querystring'),
      rotation = require('../constants/rotation.js');

const sizeEstimated = 30;

const App = React.createClass({

    sendInput: function() {
        const data = {
            priority: this.state.priority,
            name: this.state.name,
            description: this.state.description,
        };
        const success = this.fetchData.bind(this, this.serializeState(this.state));

        $.ajax({
            data: JSON.stringify(data),
            type: "POST",
            processData: false,
            contentType: 'application/json',
            "success": success,
        });
    },

    scrollTimeout: undefined,

    handleScroll: function() {
        if (this.scrollTimeout) {
            clearTimeout(this.scrollTimeout);
        }
        this.scrollTimeout = setTimeout(() => {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                const offset = (this.state.offset||0) + this.state.ammount;
                this.setState({"offset": offset}, () => {
                    this.fetchData(
                        this.serializeState(this.state),
                        true
                    )
                });

            }
        }, 1000);
    },

    deleteData: function(id) {
        const settingsStr = this.serializeState(this.state);
        const success = this.fetchData.bind(this, settingsStr);
        $.ajax({
            url: ("?id="+id),
            type: "DELETE",
            "success": success,
        });
    },

    stringifyOrderBy: function (sortDirection) {
        let sortByStr = "";
        for (sortByAttrName in sortDirection) {
            const sortDirectionPrefix = sortDirection[sortByAttrName];
            switch (sortDirectionPrefix) {
                case rotation.ASC:
                    sortByStr += sortByAttrName + ";";
                    break;
                case rotation.DESC:
                    sortByStr += "-" + sortByAttrName + ";";
                    break;
            }
        }
        return sortByStr;
    },
  
    parseOrderBy: function (orderByStr) {
        let sortDirection = {};
        while(orderByStr) {
            const orderRotation = (orderByStr.substring(0, 1) == "-") ? rotation.DESC : rotation.ASC; // Either there is rotation prefix '-' before propperty or rotation is default.
            if (orderRotation == rotation.DESC) {
                orderByStr = orderByStr.substring(1);
            }
            const offset = orderByStr.indexOf(';'); // Order by property should be all before next ';'. 
            const orderProperty = orderByStr.substring(0, offset);
            if (orderProperty != "") {
                sortDirection[orderProperty] = orderRotation;
                orderByStr = orderByStr.substring(offset+1);
            } else {
                orderByStr = ""; // At this point, all remaining data in string is considered invalid.
            }
        }
        return sortDirection;
    },

    updateEnv: function () {
        const settingsStr = this.serializeState();
        location.hash = '#'+settingsStr;
        return settingsStr;
    },

    serializeState: function() {
        const serializableSettings = {};
        const settings = this.state;
        for (let stateAttrName in settings) {
            switch (stateAttrName) {
                case "order_by":
                    const orderBy = settings["order_by"];
                    serializableSettings["order_by"] = this.stringifyOrderBy(orderBy);
                    break;
                case "tasks":
                case "fetching":
                case "ammount":
                case "offset":
                    break;
                default:
                    const stateAttrValue = settings[stateAttrName];
                    if (!(stateAttrValue == null || stateAttrValue.length === 0)) {
                        serializableSettings[stateAttrName] = settings[stateAttrName];
                    }
                    break;
            }
        }
        return queryString.stringify(serializableSettings); 
    },
    parseState: function (settingsStr) {
        const settings = queryString.parse(settingsStr);
        if (settings["order_by"]) {
            settings["order_by"] = this.parseOrderBy(settings["order_by"]);
        }
        return settings;
    },

    fetchData: function (settingsStr, doAppend) {
        const component = this;
        let offset = doAppend ? this.state.offset : 0;
        $.getJSON("?" + settingsStr + "&ammount=" + this.state.ammount + "&offset=" + offset, function(data) {
            component.setState((previousState) => {
                if (doAppend) {
                    data = Array.prototype.concat(previousState.tasks, data);
                }
                return {tasks: data, fetching: false, "offset": offset}
            });
        });
    },

    dispatch: function (deltaSettings) {
        this.setState(deltaSettings, () => this.fetchData(this.serializeState(this.state)));
    },

    
    getInitialState: function() {
        const settings = this.parseState(location.hash.substring(1));
        const ammount = Math.round($(window).height() / sizeEstimated);
        settings["ammount"] = ammount;
        settings["offset"] = 0;
        settings["tasks"] = [];
        return settings;
    },

    componentDidUpdate: function () {
        this.updateEnv();
    },

    componentDidMount: function (previousProps, previousState) {
        this.setState({fetching: true});
        this.fetchData(this.serializeState(this.state));
    },
    
    render: function () {
        $(window).scroll(this.handleScroll);
        if (this.state.fetching) {
            return <div>Loading...</div>;
        }


        return (
            <div className="container-fluid panel panel-default">
                <TasksTableHead 
                    settingsHandler={this.dispatch} 
                    priority={this.state.priority}
                    name={this.state.name}
                    description={this.state.description}
                    saveHandler={this.sendInput}
                    orderBy={this.state.order_by}
                />
                <TasksTableBody
                    tasks={this.state.tasks}
                    deleteHandler={this.deleteData}
                />
            </div>
        );
    }

});

module.exports = App;
