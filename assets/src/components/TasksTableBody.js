const React = require('react'),
      Task = require('./Task')

const TasksTableBody = React.createClass({
    render: function() {
        var tasks = this.props.tasks.map(task => <Task key={task.id} priority={task.priority} name={task.name} description={task.description} deleteHandler={this.props.deleteHandler.bind(null, task.id)} />);
        return (    
                <ul className="task-list list-group">
                    {tasks}
                </ul>
        );
    }
});

module.exports = TasksTableBody;
