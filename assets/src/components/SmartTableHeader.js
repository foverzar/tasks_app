const React = require('react'),
      rotation = require('../constants/rotation');

const SmartTableHeader = React.createClass({
    render: function() {
        let directionPrefix = "";
        if (this.props.sortDirection === rotation.DESC) {
            directionPrefix = (<span className="glyphicon glyphicon-sort-by-attributes-alt" />);
        } else if (this.props.sortDirection === rotation.ASC) {
            directionPrefix = (<span className="glyphicon glyphicon-sort-by-attributes" />);
        }
        return (
            <div className={this.props.className} onClick={this.props.onClick} >{directionPrefix} {this.props.children}</div>
        );
    },
});

module.exports = SmartTableHeader;
